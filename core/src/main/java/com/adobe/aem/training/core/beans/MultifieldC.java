package com.adobe.aem.training.core.beans;

public class MultifieldC {
    private String compname;
    private String link;
    private String compimage;

    public MultifieldC(String compname, String link, String compimage) {
        this.compname = compname;
        this.link = link;
        this.compimage = compimage;
    }

    public String getCompname() {
        return compname;
    }

    public String getLink() {
        return link;
    }

    public String getCompimage() {
        return compimage;
    }
}
