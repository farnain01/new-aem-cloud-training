package com.adobe.aem.training.core.models;

import com.adobe.aem.training.core.services.Activities;
import com.adobe.aem.training.core.services.ContentStatistics;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ActivitiesModel {

    @OSGiService
    private Activities activitiesService;

    @OSGiService
    private ContentStatistics contentStatistics;

    // Here, we get the ResourceResolver for the Logged-in User.
    // So, the permissions are dependent on the Logged-in User.
    @SlingObject
    private ResourceResolver resolver;

    private String randomActivity;

    private int assetsCount;

    private int allAssetsCount;

    @PostConstruct
    protected void init() {
        randomActivity = activitiesService.getRandomActivity();

        // Print the count for assets from the Logged-in User's ResourceResolver
        assetsCount = contentStatistics.getAssetsCount(resolver);

        // Print the total count of assets for all the logged in users
        allAssetsCount = contentStatistics.getAllAssetsCount();
    }

    public String getRandomActivity() {
        return randomActivity;
    }

    public int getAssetsCount() {
        return assetsCount;
    }

    public int getAllAssetsCount() {
        return allAssetsCount;
    }
}
