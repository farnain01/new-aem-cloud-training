package com.adobe.aem.training.core.models;

import com.adobe.aem.training.core.beans.MultifieldC;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.api.resource.Resource;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CompanyMultifieldModel {
    @ChildResource
    private Resource companies;

    List <MultifieldC> compList;

    @PostConstruct
    protected void init(){
        compList =new ArrayList<>();
        if(companies != null){
            companies.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String compname = valueMap.get("compname",StringUtils.EMPTY );
                String link = valueMap.get("link", StringUtils.EMPTY);
                String compimage = valueMap.get("compimage", StringUtils.EMPTY);
                MultifieldC compobj =new MultifieldC(compname,link,compimage);
                compList.add(compobj);
            });
        }
    }

    public List<MultifieldC> getCompList() {
        return compList;
    }
}
