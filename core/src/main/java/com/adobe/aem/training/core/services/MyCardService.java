package com.adobe.aem.training.core.services;

public interface MyCardService {
    /**
     * @return name of the organization
     */
    String getOrganizationName();

    /**
     * @return URL of the home page
     */
    String getHomepageURL();
}
