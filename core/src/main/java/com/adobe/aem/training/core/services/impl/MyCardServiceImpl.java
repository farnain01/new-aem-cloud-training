package com.adobe.aem.training.core.services.impl;

import com.adobe.aem.training.core.configs.MyCardConfiguration;
import com.adobe.aem.training.core.services.MyCardService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
        service = MyCardService.class,
        immediate = true,
        property = {
                Constants.SERVICE_ID + "=Card Service",
                Constants.SERVICE_DESCRIPTION + "=This service reads values from Card Configuration"
        })
@Designate(ocd = MyCardConfiguration.class)
public class MyCardServiceImpl implements MyCardService {
    private static final String TAG = MyCardServiceImpl.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(MyCardServiceImpl.class);

    private MyCardConfiguration configuration;

    @Activate
    protected void activate(MyCardConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public String getOrganizationName() {
        LOGGER.info("{}: reading organization name", TAG);
        return configuration.getOrganizationName();
    }

    @Override
    public String getHomepageURL() {
        LOGGER.info("{}: reading homepage url", TAG);
        return configuration.getHomepageURL();
    }


}
