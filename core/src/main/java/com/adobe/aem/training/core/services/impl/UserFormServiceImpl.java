package com.adobe.aem.training.core.services.impl;

import com.adobe.aem.training.core.beans.User;
import com.adobe.aem.training.core.services.UserFormService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;

@Component(service = {UserFormService.class})
public class UserFormServiceImpl implements UserFormService {

    private static final Logger log = LoggerFactory.getLogger(UserFormServiceImpl.class);

    private static final String JCR_PATH = "/content/usergenerated/content";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public boolean storeData(final User user) {
        ResourceResolver serviceResolver = getResourceResolver();
        boolean dataCommit = false;
        if (serviceResolver != null) {
            Resource contentFolderResource = serviceResolver.getResource(JCR_PATH);
            String alphaNumericString = RandomStringUtils.randomAlphanumeric(15);
            try {
                if (contentFolderResource != null) {
                    Resource newNode = serviceResolver.create(contentFolderResource, alphaNumericString, null);

                    // Extract the ModifiableValueMap for the Resource you want to update the properties.
                    ModifiableValueMap modifiableValueMap = newNode.adaptTo(ModifiableValueMap.class);
                    if (modifiableValueMap != null) {
                        modifiableValueMap.put("firstName", user.getFirstName());
                        modifiableValueMap.put("lastName", user.getLastName());
                        modifiableValueMap.put("email", user.getEmail());
                    }

                    // Commit all the changes
                    serviceResolver.commit();
                    dataCommit = true;
                }
            } catch (PersistenceException e) {
                log.error("PersistenceException: ", e);
            } finally {
                closeResourceResolver(serviceResolver);
            }
        }
        return dataCommit;
    }

    /**
     * Method to return the ResourceResolver Object
     *
     * @return {ResourceResolver}
     */
    private ResourceResolver getResourceResolver() {
        final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                "training-user-form"
        );
        try {
            return resourceResolverFactory.getServiceResourceResolver(authInfo);
        } catch (LoginException e) {
            log.warn("LoginException: ", e);
        }
        return null;
    }

    /**
     * Method to close the ResourceResolver Object
     *
     * @param resourceResolver ResourceResolver
     */
    private void closeResourceResolver(final ResourceResolver resourceResolver) {
        if (resourceResolver != null) {
            resourceResolver.close();
        }
    }
}
