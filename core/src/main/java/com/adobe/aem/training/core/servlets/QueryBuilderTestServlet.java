package com.adobe.aem.training.core.servlets;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component(service = {Servlet.class},
        property = {
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET,
                ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/query/test"
        })
public class QueryBuilderTestServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Reference
    private QueryBuilder queryBuilder;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        // 1. Get the Search Keyword via Query Param
        String searchKeyword = request.getParameter("searchKeyword");
        response.setContentType("application/json");

        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);

        Map<String, String> predicates = new HashMap<>();
        predicates.put("path", "/content/wknd");
        predicates.put("type", "cq:Page");
        predicates.put("1_property", "jcr:content/jcr:title");
        predicates.put("1_property.value", searchKeyword);

        //predicates.put("fulltext", searchKeyword);

        Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        SearchResult searchResult = query.getResult();

        JsonObject responseJson = new JsonObject();

        searchResult.getResources().forEachRemaining(item -> {
            Page page = item.adaptTo(Page.class);
            if (page != null) {
                responseJson.addProperty(page.getTitle(), page.getPath().concat(".html"));
            }
        });

        response.getWriter().println(new Gson().toJson(responseJson));
        response.flushBuffer();
    }
}
