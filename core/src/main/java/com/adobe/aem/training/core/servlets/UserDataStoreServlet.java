package com.adobe.aem.training.core.servlets;

import com.adobe.aem.training.core.beans.User;
import com.adobe.aem.training.core.services.UserFormService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

@Component(service = {Servlet.class})
@SlingServletResourceTypes(
        resourceTypes = "training/components/content/userform",
        methods = HttpConstants.METHOD_POST,
        extensions = "json"
)
@ServiceDescription("User Form Data Store Servlet")
public class UserDataStoreServlet extends SlingAllMethodsServlet {

    protected static final String APPLICATION_JSON = "application/json";

    @Reference
    private transient UserFormService userFormService;

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType(APPLICATION_JSON);

        // 1. Get the User data from the HTTP POST Request
        String requestObject = IOUtils.toString(request.getReader());
        Gson gson = new Gson();
        User userObject = gson.fromJson(requestObject, User.class);

        // 2. Store the data in JCR
        JsonObject responseObject = new JsonObject();
        boolean isDataStored = userFormService.storeData(userObject);
        if (isDataStored) {
            responseObject.addProperty("status", 200);
            responseObject.addProperty("message", "Record has been successfully stored");
        } else {
            responseObject.addProperty("status", 500);
            responseObject.addProperty("message", "Something went wrong!");
        }

        // 3. Return the JSON Response
        out.println(responseObject);
        response.flushBuffer();
    }
}
