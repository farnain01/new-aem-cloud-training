(function ($, document, ns) {
    $(document).on("dialog-ready", function() {
		const showHideImage = function(e) {
            if($("[name='./hideimage']").prop("checked")) {
                $("input[name='./productimage']").closest(".coral-Form-fieldwrapper").hide();
            } else {
                $("input[name='./productimage']").closest(".coral-Form-fieldwrapper").show();
            }
        };
		$(document).on("change", "[name='./hideimage']", showHideImage);
		showHideImage();
    });
})(Granite.$, document, Granite.author);